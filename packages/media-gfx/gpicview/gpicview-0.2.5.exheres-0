# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=lxde suffix=tar.xz ] \
    freedesktop-desktop \
    gtk-icon-cache \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A Simple and Fast Image Viewer for X"
HOMEPAGE+=" http://lxde.sourceforge.net/${PN}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: af ar ast be bg bn bn_IN bs ca cs da de el en_GB eo es et eu fa fi fo fr gl he hr hu
               id is it ja kk kn ko lg lt ml ms nb nl nn pa pl ps pt pt_BR ro ru sk sr sr@latin sv
               te th tr tt_RU ug uk ur ur_PK vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        dev-util/intltool[>=0.40.0]
    build+run:
        x11-libs/gtk+:3
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    post:
        x11-apps/xdg-utils
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.2.3-desktop-file.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-gtk3
    --enable-nls
    --disable-static
)

src_prepare() {
    # respect datarootdir
    edo sed \
        -e 's:itlocaledir = $(prefix)/$(DATADIRNAME)/locale:itlocaledir = $(datarootdir)/locale:' \
        -i po/Makefile.in.in
    edo sed \
        -e 's:$(prefix)/$(DATADIRNAME)/locale:$(datarootdir)/locale:g' \
        -i src/Makefile.am

    autotools_src_prepare
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

